import booking from './booking';
import confirmedBooking from './confirmedBooking';

export const schemaTypes = [
  {
    name: 'author',
    type: 'document',
    title: 'Author',
    fields: [
      {
        name: 'author',
        type: 'string',
        title: 'Author',
      },
      {
        name: 'biografia',
        title: 'Biografia',
        type: 'array',
        of: [
          {
            type: 'block',
          },
          {
            type: 'image',
            fields: [
              {
                title: 'Position',
                name: 'position',
                type: 'string',
                options: {
                  list: [
                    {title: 'Center', value: 'center'},
                    {title: 'Left', value: 'left'},
                    {title: 'Right', value: 'right'},
                  ],
                  layout: 'radio',
                  isHighlighted: true,
                },
              },
              {
                type: 'text',
                name: 'alt',
                title: 'Description',
                options: {
                  isHighlighted: true,
                },
              },
            ],
            options: {
              hotspot: true,
            },
          },
        ],
      },
      // {
      //   name: "slug",
      //   title: "Slug",
      //   type: "slug",
      //   options: {
      //     source: (document, options) => {
      //       const query = '*[_type=="category" && _id == $ref]{...}';
      //       const params = { ref: document.category._ref };
      //       const cattitle = client.fetch(query, params).then(data => {
      //         console.log("fetched", data);
      //         return data[0].title;
      //       });
      //       return cattitle;
      //     },
      //     slugify: input => {
      //       return input
      //         .toLowerCase()
      //         .replace(/\s+/g, "-")
      //         .replace(/[^\w-]+/g, "");
      //     },
      //     isUnique: true
      //   }
      // }
    ],
  },
  {
    name: 'project',
    type: 'document',
    title: 'Projcect Name o Collezione',
    fields: [
      {
        name: 'name',
        title: 'Name',
        type: 'string',
      },
      {
        name: 'tecnica',
        title: 'Tecnica es:(olio su tela)',
        type: 'string',
      },
      {
        name: 'author',
        title: 'Author',
        type: 'reference',
        to: [{type: 'author'}],
        validation: (Rule) => Rule.required(),
        // initialValue: () => ({
        //     author: [
        //       {
        //         _type: "authorReference",
        //         author: {
        //           _type: 'author',
        //           name: 'Antonio Pio Cavaliere',
        //           _id: '94eebc86-1194-4643-a05b-1aae46c798b6',
        //           _updatedAt: '2023-11-14T08:27:12Z',
        //           _createdAt: '2023-11-14T08:27:12Z',
        //           _rev: 'iRIXAgGwoOqHoEoAUhXZDN'
        //         }
        //       }
        //     ]
        //   }),
      },
      {
        name: 'coverImage',
        title: 'Immagine Copertina',
        type: 'image',
        fields: [
          {
            type: 'string',
            name: 'alt',
            title: 'ImageTitle',
            options: {
              isHighlighted: true,
            },
          },
        ],
      },
    ],
  },
  {
    name: 'title',
    type: 'document',
    title: 'Testi',
    fields: [
      {
        name: 'title',
        title: 'Titolo',
        type: 'string',
      },
      {
        name: 'shortContent',
        title: 'Anteprima',
        type: 'array',
        of: [{type: 'block'}],
      },
      {
        name: 'content',
        title: 'Content',
        type: 'array',
        of: [{type: 'block'}],
      },
      {
        name: 'author',
        title: 'Author',
        type: 'reference',
        to: [{type: 'author'}],
        validation: (Rule) => Rule.required(),
      },
    ],
  },
  {
    name: 'gallery',
    type: 'document',
    title: 'Gallery',
    fields: [
      {
        name: 'title',
        type: 'string',
        title: 'Title',
      },
      {
        name: 'project',
        title: 'Progetto',
        type: 'reference',
        to: [{type: 'project'}],
        validation: (Rule) => Rule.required(),
      },
      {
        name: 'images',
        type: 'array',
        title: 'Images',
        of: [
          {
            name: 'image',
            type: 'image',
            title: 'Image',
            options: {
              hotspot: true,
            },
            fields: [
              {
                name: 'alt',
                type: 'string',
                title: 'Titolo',
              },
              {
                name: 'dimension',
                type: 'string',
                title: 'Dimension Lun*Alt',
              },
            ],
          },
        ],
        options: {
          layout: 'grid',
        },
      },
      {
        name: 'author',
        title: 'Author',
        type: 'reference',
        to: [{type: 'author'}],
        validation: (Rule) => Rule.required(),
      },
    ],
  },
  booking,
  confirmedBooking,
]
