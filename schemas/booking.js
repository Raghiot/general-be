export default {
  name: 'booking',
  title: 'Booking',
  type: 'document',
  fields: [
    { name: 'firstName', title: 'First Name', type: 'string' },
    { name: 'lastName', title: 'Last Name', type: 'string' },
    { name: 'email', title: 'Email', type: 'string' },
    { name: 'startDate', title: 'Start Date', type: 'datetime' },
    { name: 'endDate', title: 'End Date', type: 'datetime' },
    {
      name: 'roomType',
      title: 'Room Type',
      type: 'string',
      options: {
        list: [
          { title: 'Classic', value: 'classic' },
          { title: 'Suite', value: 'suite' },
        ],
      },
    },
    {
      name: 'status',
      title: 'Status',
      type: 'string',
      options: {
        list: [
          { title: 'Pending', value: 'pending' },
          { title: 'Accepted', value: 'accepted' },
          { title: 'Declined', value: 'declined' },
        ],
      },
      initialValue: 'pending',
    },
    {
      name: 'bookingNumber',
      title: 'Booking Number',
      type: 'number',
      description: 'Unique booking number incremented automatically',
      validation: (Rule) => Rule.positive().integer(),
    },
  ],
};
