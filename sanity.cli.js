import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'lt6ban25',
    dataset: 'production'
  }
})

