import {defineConfig} from 'sanity'
import {deskTool} from 'sanity/desk'
import {visionTool} from '@sanity/vision'
import {schemaTypes} from './schemas'
import { vercelDeployTool } from 'sanity-plugin-vercel-deploy'

export default defineConfig({
  name: 'default',
  title: 'General Backend',

  projectId: 'lt6ban25',
  dataset: 'production',

  plugins: [deskTool(), visionTool(),vercelDeployTool()],

  schema: {
    types: schemaTypes,
  },
})
